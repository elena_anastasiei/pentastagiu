import math

radius = float(input('Radius of the circle: '))

circle_area = round(math.pi * math.pow(radius, 2),2)
print('A = %.2f' % circle_area)