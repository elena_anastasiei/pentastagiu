s = str(input('s = '))
new_s = ''
for i in s:
    letter = ord(i)
    if (letter > 96 and letter < 123):
        letter = letter - 32
        new_s+=chr(letter)
    elif( letter > 64 and letter < 91):  
        letter = letter + 32
        new_s+=chr(letter)
    else:
        new_s+=i

print(new_s)
