Dictionar = {
    "apple": "mar",
    "car":"masina",
    "bike":"bicicleta",
    "beer":"bere"
}

cuvant = input('cuvant: ')

if (cuvant in Dictionar):
    print("Traducerea este: " + Dictionar[cuvant])
else:
    print("Nu eista in dictionar!")