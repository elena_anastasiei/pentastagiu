numere = {
    0:"zero zero",
    1:"unu",
    2:"doi",
    3:"trei",
    4:"patru",
    5:"cinci",
    6:"sase",
    7:"sapte",
    8:"opt",
    9:"noua",
    10:"zece",
    11:"unsprezece",
    12:"doisprezece",
    13:"treispezece",
    14:"paisprezece",
    15:"cincisprezece",
    16:"saisprezece",
    17:"saptesprezece",
    18:"optsprezece",
    19:"nousprezece",
    20:"douazeci",
    30:"treizeci",
    40:"patruzeci",
    50:"zeci",
    60:"saizeci",
    ":":"si"
}
ora = input('ora -> format hh:mm = ')
h = ora.split(":")[0]
m = ora.split(":")[1]

def check_hm(h, m):
    if (int(h) > 23 or int(m) > 60 or int(h) < 0 or int(m) < 0):
        return False
    else:
        return True

def search_hour(n):
    if int(n) in numere:
        ora = numere[int(n)]
    else:
        ora = numere[int(n[0])*10]+' si '+ numere[int(n[1])]
    return ora

if(check_hm(h, m)):    
    print(search_hour(h)+' si ' + search_hour(m))
else:
    print('formatul orei este gresit')