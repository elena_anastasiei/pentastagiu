import sys

def vocale(c):
    v = ['a', 'o', 'i', 'e', 'u']
    c = c.lower()
    n = 0
    for char in c:
        if char in v:
            n += 1
    return n
    
def frecventa_litere(c):
    litere = {}
    for char in c:
        if char in litere:
            litere[char] += 1
        else:
            litere[char] = 1
    return litere
    
if (len(sys.argv) > 1):
    cuvinte = sys.argv[1:]
    for c in cuvinte:
        print('In cuvantul {} sunt {} vocale'.format(c, vocale(c)))
    for key, value in frecventa_litere(''.join(cuvinte)).items():
        print('Literea {} apare de {}'.format(key,value))
        
    
