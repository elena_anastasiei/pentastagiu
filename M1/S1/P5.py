def is_triunghi(a, b, c):
    if (a > 0 and b > 0 and c > 0) and ((a + b > c) or (b + c > a) or (c + a > b)):
        return True
    else:
        return False
    
def tip_triunghi(a, b, c):
    if a == b and b == c:
        return 'echilateral'
    elif a == b or b == c or c == a:
        return 'isoscel'
    else:
        return 'oarecare'
    
a ,b, c = float(input('A = ')), float(input('B = ')), float(input('C = '))

if is_triunghi(a, b, c):
    print(tip_triunghi(a ,b ,c))
else:
    print('invalid')
