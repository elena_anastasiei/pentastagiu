##def p1(*args):
##    return sum(args)
import doctest

"""
Return perimeter of a polygon by adding together the length
of each side which are greater then > 0
"""

def p1(*args):
    """
    Calculate the perimeter if a polygon using recursion
    >>> p1(1,2,3)
    6
    >>> p1(1,4,5,6,7)
    23
    >>> p1(98,234,1)
    333
    >>>
    
    """
    if(all(i > 0 for i in args)):
        if len(args) == 1:
            return args[0]
        else:
            return args[-1] + p1(*tuple(args[:-1]))
    else:
        print('All numbers should be > 0')

if __name__ == "__main__": 
    doctest.testmod()
    
