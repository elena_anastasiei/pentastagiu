from collections import defaultdict

contacte = defaultdict(list)

def afiseaza_nr(d):
    for key, val in d.items():
            print('{} - {}'.format(key, val) )
            
def afiseaza(d):
    
    print(55*'*'+'\nNume Prenume : Telefon 1 - 1234, Telefon 2 - 5678 etc.')
    
    for k,v in d.items():
        print('\n{} : '.format(k))
        afiseaza_nr(v)
    print(55*'*')
    
def adauga(d):
    
    key = input('Nume Prenume: ')
    value = input('Telefon 1, Telefon 2 etc.: ')
    try:
        d[key] = defaultdict(int)
    
        for i, x in enumerate([int(x) for x in value.split(',')]):
            d[key]['Telefon '+ str(i+1)] = int(x)
    except:
        print('Numerele de telefon trebuie sa contina doar cifre')
    
def p10():
    afiseaza(contacte)
    raspuns = ''
    while(raspuns!= '3'):
        menu = '1. adauga contact\n'
        menu += '2. editeaza contact\n'
        menu += '3. iesire\n'
        raspuns = input(menu + 'tasteaza 1, 2 sau 3 : ')
        
        if raspuns == '1':
            print(8*'*'+'\nadauagre\n'+8*'*')
            adauga(contacte)
            afiseaza(contacte)
        if raspuns == '2':
            
            print(8*'*'+'\neditare\n'+8*'*')
            key = input('Nume Prenume: ')
            try:
                afiseaza_nr(contacte[key])
                n_telefon = len(contacte[key])
                numar = input('Introduceti numarul telefonului care va fi editat din cele {} : '.format(n_telefon))
                if (int(numar) <= n_telefon):
                    try:
                        nr_nou = int(input('Intoruceti noul numar de telefon : '))
                        contacte[key]['Telefon '+ numar] = nr_nou
                        afiseaza(contacte)
                    except:
                        print('Numarul trebuie sa contina doar cifre')
                else:
                    print('Numarul introdus este gresit\n')
            except:
                print('\nContact incexistent\n')
                
            
            

