import doctest

"""
The p11 function takes 2 arguments: nume set with default value as "cutarescu",
prenume set with default value as "cutareasca" and a list of key = value arguments
and return a dict = {nume: value, prenume: value, 1...* kwargs = kwargs[value]}
"""
def p11(nume = "cutarescu", prenume = "cutareasca", **kwargs):
    """
    >>> from P11 import p11
    >>> p11('Lupu','Elena',varsta = 26,statut = 'casatorita')
    {'nume': 'Lupu', 'prenume': 'Elena', 'varsta': 26, 'statut': 'casatorita'}
    >>> p11()
    {'nume': 'cutarescu', 'prenume': 'cutareasca'}
    >>> p11(ana="are", multe="mere")
    {'nume': 'cutarescu', 'prenume': 'cutareasca', 'ana': 'are', 'multe': 'mere'}
    >>> p11('Stiu', ca="vine")
    {'nume': 'Stiu', 'prenume': 'cutareasca', 'ca': 'vine'}
    >>>
    
    """
    d = {}
    d['nume'] = nume
    d['prenume'] = prenume
    for k in kwargs:
        d[k] = kwargs[k]
    return d
    
if __name__ == "__main__": 
    doctest.testmod()