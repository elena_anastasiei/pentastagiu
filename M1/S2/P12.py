def p12(arg):
    if(type(arg) is int):
        return lambda x, y: 1 if x > y else (0 if x == y else -1)
    elif(type(arg) is str):
        return lambda x, y: 1 if len(x) > len(y) else (0 if len(x) == len(y) else -1)
