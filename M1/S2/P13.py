from collections import OrderedDict, Counter
from datetime import datetime
def cantar(n_zile = 7):
    p = OrderedDict()
    g_tinta= input('Greutatea tinta este: ')
    while(n_zile):
        p_date = persoana(g_tinta)
        p[p_date[0]] = p_date[1:]
        n_zile = n_zile - 1
    sorted_p = (OrderedDict(sorted(p.items(), key = lambda t: t[0])))
    for k in sorted_p:
        print('\ndata: {}'.format(k))
        print('greutatea tinta: {}'.format(sorted_p[k][0]))
        print('greutatea actuala: {}'.format(sorted_p[k][1]))
        print('{} kg'.format(sorted_p[k][2]['dif']))
    
def persoana(g_tinta):
    d = str(input('Introduceti data in format dd/mm/yy: '))
    try:
        data = datetime.strptime(d, "%d/%m/%y").date()
    except:
        data = '00/00/00'
        print('Formatul datei introduse este gresit')
        
    g_act = float(input('Greutatea actuala: '))
    g = float(g_tinta) - g_act
    c = Counter()
    c.update(dif = g)
    if(+c):
        print('\n\nIn data de {} '.format(data))
        print('greutatea actuala de {} kg '.format(g_act))
        print('era cu {} kg '.format(c['dif']))
        print('sub nivelul propus {} kg\n\n'.format(float(g_tinta)))
    return data, float(g_tinta), g_act, c
