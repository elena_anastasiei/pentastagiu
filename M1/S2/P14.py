def p14(n, t1, intermediar, t2):
     if n > 0:
        p14(n-1,t1, t2, intermediar)
        if t1:
            t2.append(t1.pop())
        print('Turn 1  -> {} Intermediar -> {} Turn 2 -> {}'.format(t1, intermediar, t2))
        p14(n-1,intermediar,t1,t2)
        
n = int(input('Numarul de dicuri:'))
t1 = [x+1 for x in range(n)][::-1]
intermediar = []
t2 = []
p14(n,t1, intermediar, t2)
