def p15(n):
    matrice  = [x for x in range(2, n+1)]
    prime = []
    while matrice:
        y = matrice[0]
        matrice = list(filter(lambda x, y = y: x%y > 0, matrice))
        prime.append(y)
    print(prime)
