##def p2(*args):
##    return (min(args), max(args))

import doctest
"""
The p2 function return the maximun and the minimum number
from a list of numbers given as arguments of the p2 function
"""

def p2(*args):
    """
    Calculate the minimum and maximum element of a list of numbers
    >>> p2(0)
    (0, 0)
    >>> p2(-1,-2)
    (-2, -1)
    >>> p2(10,0,-2,8)
    (-2, 10)
    >>> p2(2.2, -2.2,3.3, -3.3,0)
    (-3.3, 3.3)
    >>> 
    
    """
    return min(args, key = lambda x:x), max(args, key = lambda x:x)
    
if __name__ == "__main__": 
    doctest.testmod()

    
