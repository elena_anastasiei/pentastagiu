import doctest
""" Return sum of numbers from 1 to n 
where n type int is given as a parameter to p3 function
>>>p3(3)
6
"""
def p3(n):
    """
    Calculate the sum from 1 to n numbers using recurision
    
    >>> p3(0)
    0
    >>> p3(1)
    1
    >>> p3(10)
    55
    >>>
    """
    if n == 0:
        return 0
    else:
        return n + p3(n-1)
                
if __name__ == "__main__": 
    doctest.testmod()
        