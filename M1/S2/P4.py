import doctest
"""
The function p4 a number > 0 as argument and print n*, n-1*,...,* stars
"""
def p4(n):
    """
    >>> p4(3)
    *
    **
    ***
    >>> p4(1)
    *
    >>> p4(10)
    *
    **
    ***
    ****
    *****
    ******
    *******
    ********
    *********
    **********
    >>>

    """
    if( n > 1):
        p4(n-1)
    print(n*'*')
    
if __name__ == "__main__": 
    doctest.testmod()