##from collections import deque
##def p5(c):
##    print(deque(c))
##    print(deque(reversed(c)))
##    if (deque(c) == deque(reversed(c))):
##        return True
##    else:
##        return False

from collections import deque
def p5(c):
    c = deque(c)
    while (len(c) > 1):
        if(c.popleft() == c.pop()):
            continue
        else:
            return False
    else:
        return True
