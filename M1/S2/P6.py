def p6(c, l):
    c = list(c)
    if len(c) == 0:
        print('L = ', end='')
        return l
    else:
        l.append(c[0].upper())
        return p6(c[1:],l)
