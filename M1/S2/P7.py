import re

def p7(f, iterabil):
    tip = re.search('\'([^\']*)',str(type(iterabil))).group(1)
    if tip == 'list':
          return [f(x) for x in iterabil]
    elif tip == 'tuple':
          return tuple([f(x) for x in iterabil])
    elif tip == 'str':
          return ''.join([f(x) for x in iterabil])
    elif tip == 'dict':
        for key,value in iterabil.items():
            iterabil[key] = f(value)
        return iterabil
    else:
        return 'Couldn\'t perform the requested'
