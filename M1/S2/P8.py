from functools import reduce

def p8(f_comp, l):
    sort_l = []
    while l:
        x =  reduce(lambda x,y: y if f_comp(x,y) > 0 else x, l)
        l.remove(x)
        sort_l.append(x)
    return sort_l

def f_comp(a,b):
    if (a > b):
        return 1
    elif (a < b):
        return -1
    else:
        return 0
    
