def p9(f, *args, **keyw):
    s = keyw['start']
    p = keyw['pas']
    lista = args[0]
    return list(map(f, lista[s::p]))
