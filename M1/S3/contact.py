class Contact:
    def __init__(self, name, phone):
        self.name = name
        self.phonelist = [phone]
        
    def show_contact(self):
        print('\n{} :'.format(self.name))
        for i in self.phonelist:
            i.show_phone()
            
    def add_phone(self, phone):
        self.phonelist.append(phone)
