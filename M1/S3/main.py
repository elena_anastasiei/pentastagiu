from phone import Phone
from contact import Contact
from phonebook import Phonebook
import re

def menu():
    my_phone_book = Phonebook()
    command = ''
    exit_command = -1
    
    while (command != '0'):
        my_menu = '\n1. Add contact\n2. Search contact'
        my_menu += '\n3. Edit contact\n4. Delete contact'
        my_menu += '\n5. Show phonebook\nPress 1,2,3,4,5 or 0 to quit the phonebook: '
        command = ''
        command = input(my_menu)
        
        if command == '1':
            exit_command = ''
            while( exit_command != '0'):
                name = input('Enter the name of the new contact: ')
                
                if not re.match("^[A-Za-z ]*$", name):
                    print('Only letter are allowed. Please try again.')
                    continue
                    
                type = input('Enter the type fo the phone number: ')
                
                while(exit_command != '0' ):
                    
                    number = input('Enter the phone number: ')
                    
                    if not re.match("^[0-9\(\)\+\s]+$", number):
                        print('Only number, spaces, +, ( and ) are allowed. Please try again.')
                        continue
                        
                    #exit_command = input('Do you want to add another phone number to this contact? If not, then press 0')
                    exit_command = '0'
                    
                new_contact = Contact(name, Phone(type, number))
                my_phone_book.add_contact(new_contact)
                my_phone_book.show_phonebook()
                
        elif command == '2':
            name = input('\nEnter the name of the contact to edit: ')
            my_phone_book.serch_contact(name)
            
        elif command == '3':
            name = input('\nEnter the name of the contact to edit: ')
            my_phone_book.edit_contact(name)
            my_phone_book.show_phonebook()
            
        elif command == '4':
            name = input('\nEnter the name of the contact to delete: ')
            my_phone_book.delete_contact(name)
            my_phone_book.show_phonebook()
            
        elif command == '5':
            my_phone_book.show_phonebook()
            
        elif command != '0':
            print('\nUnexistent command. Please try again.')
        else:
            print('...')

menu()