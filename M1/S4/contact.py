from phone import Phone
import re

class Contact:
    def __init__(self, name, email, *phone):
        self.name = self.validate_name(name)
        self.email = self.validate_email(email)
        self.phonelist = phone  #a list of Phone objects
        
    def show_contact(self):
        if self.email != '':
            print('\nContact: {}\nemail: {}'.format(self.name, self.email))
        else:
            print('\nContact: {}'.format(self.name))
            
        for i in self.phonelist:
            i.show_phone()
            
    def add_phone(self, phone):
        self.phonelist.append(phone)
        
    def validate_email(self, email):
        if email !='':
            while(not re.match("^[\w\.]+@(?:[\w]+\.)[a-zA-Z]{2,4}$",email)):
                email = input('Wrong email format. Try again: ')
        
        return email
        
    def validate_name(self,name):
        while(not re.match("^[A-Za-z ]*$", name)):
            name = input('Wrong name format. Only letter are allowed. Please try again: ')

        return name