from phone import Phone
from contact import Contact
from phonebook import Phonebook
import re, pickle

def menu():
    try:
        with open('saved_phonebook.pkl','rb') as inputFile:
            my_phone_book = pickle.load(inputFile)
    except:
        my_phone_book = Phonebook()
        
    command = ''
    
    while (command != '0'):
        my_menu = '\n1. Add contact\n2. Search contact'
        my_menu += '\n3. Edit contact\n4. Delete contact'
        my_menu += '\n5. Show phonebook\n 1,2,3,4,5 or 0 to quit the phonebook: '
        
        command = ''
        command = input(my_menu)
        
        if command == '1':
            contact_name = input('Add name: ')
            contact_email = input('Add email: ')
            phone_number_type = input('Add phone number type: ')
            phone_number = input('Add phone number: ')
            
            new_phone = Phone(phone_number_type, phone_number)
            new_contact = Contact(contact_name, contact_email, new_phone)
            
            my_phone_book.add_contact(new_contact)
            my_phone_book.show_phonebook()
                
        elif command == '2':
            contact_name = input('\nContact name: ')
            my_phone_book.serch_contact(contact_name)
            
        elif command == '3':
            contact_name = input('\nContact name: ')
            my_phone_book.edit_contact(contact_name)
            my_phone_book.show_phonebook()
            
        elif command == '4':
            contact_name = input('\nContact name: ')
            my_phone_book.delete_contact(contact_name)
            my_phone_book.show_phonebook()
            
        elif command == '5':
            my_phone_book.show_phonebook()
            
        elif command != '0':
            print('\nUnexistent command. Please try again.')
            
        else:
            with open('saved_phonebook.pkl', 'wb+') as output:
                pickle.dump(my_phone_book,output)

menu()
