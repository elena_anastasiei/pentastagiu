import re

class Phone:
    def __init__(self, type, number):
        self.type = type
        self.number = self.validate_number(number)
         
    def show_phone(self):
        print('{} : {}'.format(self.type, self.number))
        
    def validate_number(self, number):
        while( not re.match("^(112|333|433|((\((((00|\+)\d{2})|0)\)|((00|\+)\d{2}|0))(\s*[0-9]{3}){3})$)",number)):
            number = input('Wrong number format. Please try a different number: ')
            
        return number