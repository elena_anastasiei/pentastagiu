from phone import Phone
from contact import Contact

class Phonebook:
    def __init__(self):
        self.allphones = {}
        self.__add_default_numbers()
    
    def add_contact(self, contact):
        self.allphones[contact.name] = contact
        
    def __add_default_numbers(self):
        self.add_contact(Contact("Emergency",'',Phone("Emergency", '112')))
        self.add_contact(Contact("Service Assistance",'', Phone("Service assistance", '433')))
        self.add_contact(Contact("Credit",'', Phone("Credit", '333')))
        
    def delete_contact(self, name):
        del self.allphones[name]
        
    def show_phonebook(self):
        keys_array = list(self.allphones.keys())
        status  = 'n'
        page_size = 5
        first = 0
        
        while(status != 'q'):
            for i in keys_array[first:first+page_size]:
                self.allphones[i].show_contact()
        
            first = first+page_size
            
            if len(keys_array) < 6:
                status = 'q'
            else:
                status = input('\nPress ENTER for more contacts or q for exist: ')
            
    def serch_contact(self, name):
         try:
            current_contact = self.allphones[name]
            current_contact.show_contact()
         except:
            print('Couldn\'t find the contact')
            
    def edit_contact(self, name):
        try:
            current_contact = self.allphones[name]
            current_contact.show_contact()
            result = input('1.Edit name\n2.Edit number\n3.Add another number\n4.Edit email\nPress 1,2,3 or 4: ')
            
            if '1' in result:
                result = input('New name: ')
                self.delete_contact(name)
                current_contact.name = result
                self.add_contact(current_contact)
                
            elif '2' in result:
                current_contact.show_contact()
                result = input('Select the type of the phone number: ')
                
                for obj in current_contact.phonelist:
                    if obj.type == result:
                        result = input('\n1.Edit type\n2.Edit number\nPress 1 or 2: ')
                        
                        if '1' in result:
                            result = input('New type: ')
                            obj.type = result
                        elif '2' in result:
                            result = input('New number: ')
                            obj.number = result
                        else:
                            print('Unavailable')
                            
            elif '3' in result:
                current_contact.show_contact()
                type = input('Enter the type of the number: ')
                number = input('Enter the phone number: ')
                current_contact.add_phone(Phone(type, number))
                current_contact.show_contact()
                
            elif '4' in result:
                current_contact.show_contact()
                email = input('Enter new email adress: ')
                current_contact.email = email
            else:
                print('Unavailable')
                
        except:
            print('Couldn\'t find the contact')
            
