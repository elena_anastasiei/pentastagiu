from candidate import Candidate
from faculty import Faculty
from student import Student

class Admission:
    def __init__(self, date, university, faculty, candidates):
        self.date = date
        self.university = university
        self.faculty = faculty
        self.faculty.add_students(self.admission_process(candidates))

    def admission_process(self, candidates):
        students = {}
        for i in candidates:
            if self.validate_candidate(i):
                Faculty.student_id +=1
                ##!!!!tre sa verificam daca are mai multe dosare si sa luam cel necesar
                students[Faculty.student_id] = Student(i.name, Faculty.student_id,  i.admission_files['info'], 1)
        return students
            
    def admission_grade(self, candidate):
        try:
            return (candidate.grades['matematica']+candidate.grades['informatica'])/2
        except:
            return 0
     

    def validate_admission_files(self, candidate):
        #not completed
        return True
       
        
    def validate_candidate(self, candidate):
        if (self.admission_grade(candidate) >= 5.0 and self.validate_admission_files(candidate)):
            return True


     
    def show_admission(self):
        print("Admission date: {}\n".format(self.date))
        self.university.show_university()
        self.faculty.show_faculty()
        self.faculty.show_students()
        
 ##       for i in range(len(self.candidates)):
 ##           print('Candidate {}'.format(i+1))
 ##           self.candidates[i].show_candidate()

