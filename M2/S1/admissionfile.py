class Admissionfile:
    def __init__(self, name, files):
        self.name = name #name of the admission file (for which faculty)
        self.files = files #list of documents required by that faculty

    #add another document to the admission file list of documents
    def add_file(self, file):
        self.files.append(file)

    #remove a document from the admission file list of documents
    def remove_file(self, file):
        if file in self.files:
            self.files.remove(file)
            print("File \"{}\" was successefully removed from \"{}\" admission file".format(file, self.name))
        else:
            print("File \"{}\" is not in the \"{}\" admission file".format(file, self.name))
            
    #show admission file content
    def show_files(self):
        print("Admission file name \"{}\" containes the following documents: ".format(self.name))
        
        for i, item in enumerate(self.files):
            print("{}. {}".format(i+1,item))