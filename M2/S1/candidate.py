from admissionfile import Admissionfile

class Candidate:
    def __init__(self, name, grades, admission_files):
        self.name = name #name of the candidate
        self.grades = grades #a dictionary {} with the grades of the candidate
        self.admission_files = {}
        for i in admission_files: 
            #saving the list of Admissionfile objects into a dictionary with 
            #the admission file name as key and Admissionfile object as value
            self.admission_files[i.name] = i

    #show candidate info: name and its admission files
    def show_candidate(self):
        print("Candidate name: {}\n".format(self.name))
        
        for key in self.admission_files:
            self.admission_files[key].show_files()
            print("\n")