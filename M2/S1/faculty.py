from student import Student
class Faculty:
    max_students = 2 
    student_id = 200000000
    
    def __init__(self, name, street_adress, email, phone_number):
            self.name = name #faculty name
            self.street_adress = street_adress #faculty adress
            self.email = email #faculty email
            self.phone_number = phone_number #faculty phone number
            self.students = [Student('','','','')]
            
    #show faculty details
    def show_faculty(self):
        print("Faculty: {}\nStreet adress: {}".format(self.name, self.street_adress))
        print("Email: {}\nPhone number: {}\n".format(self.email, self.phone_number))
        
    #add students to the faculty in admission class
    def add_students(self, students):
        self.students = students #dictionay{student_id:Student object...}
    
    #remove a student from faculty student list
    def remove_student(self, student):
        del self.students[student.student_id]
        
    def show_students(self):
        for key in self.students:
            self.students[key].show_student()
        