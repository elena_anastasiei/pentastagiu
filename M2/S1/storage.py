import pickle

class Storage:

    def __init__(self, filename):
        self.filename = filename+".pkl"
        
    def  pack_obj(self, *obj_list):
        with open(self.filename, 'wb') as output:
            pickle.dump(obj_list, output, pickle.HIGHEST_PROTOCOL)
            del obj_list

    def unpack_obj(self):
        with open(self.filename, 'rb') as input:
            obj_list = pickle.load(input)
        return obj_list