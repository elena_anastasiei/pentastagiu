from admissionfile import Admissionfile

class Student:
    def __init__(self, name, student_id, student_files, year): #,courses):
        self.name = name #student name
        self.student_id = student_id #student id at the faculty
        self.student_files = student_files #student documents at the faculty, object of Admissionfile class
        self.year = year
        #self.courses = courses #object of a Cource class
        
    #show student data
    def show_student(self):
        print("Student name: {}\nStudent id: {}\nStudent year: {}".format(self.name, self.student_id, self.year))
        self.student_files.show_files()