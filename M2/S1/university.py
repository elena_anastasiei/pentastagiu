import json

from faculty import Faculty

class University:
    def __init__(self, name, country, city, faculties):
            self.name = name #university name
            self.city = city #university city
            self.country = country #university country
            self.faculties = faculties #university list of faculties, objects of Faculty class

    #show university data
    def show_university(self):
        print("Univercity: {}\nCountry: {}\nCity: {}".format(self.name, self.country, self.city))
        print("\nList of faculties:\n")
        
        for i in self.faculties:
            i.show_faculty() 
    
    #add new faculty to the university, object of a Facuty class
    def add_faculty(self, faculty):
        self.faculties.append(faculty)
    
    #remove a faculty from the university, object of a Facuty class
    def remove_faculty(self, faculty):
        if faculty in self.faculties:
            self.faculties.remove(faculty)
