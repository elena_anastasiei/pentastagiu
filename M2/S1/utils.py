
##Menu list, user for menu.py module
MENU_ITEMS = ['ADD/SHOW University/ies', 'ADD/SHOW faculty/ies', 'ADD Candidates for admission', 'REMOVE Candidates from admission', 'SHOW admission candidates (5 per page)', 'SHOW Admission result']

##Data location and filename, used for storage.py module
univer_filename = 'data/universities_data'
facult_filename = 'data/faculties_data'
candid_filename = 'data/candidates_data'
studen_filename = 'data/students_data'

##Admission files, standart for all universities
def show_admission_info():
    with open('data/admission_info.txt', 'r') as f:
        print(f.read())